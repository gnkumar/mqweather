//
//  SharedInstance.swift
//  MQCodeChallenge
//
//  Created by Apple on 15/06/21.
//

import Foundation
import UIKit


class ShareInstance {
    
    static let shared = ShareInstance()
       init(){}
    var locationData = [LocationData]()
}

struct LocationData {
    var lattitude: Double?
    var longitude: Double?
    var locName: String?
    init(locLatitude: Double, locLongitude: Double, name: String) {
        self.lattitude = locLatitude
        self.longitude = locLongitude
        self.locName = name
    }
}
