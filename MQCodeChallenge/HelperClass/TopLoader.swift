//
//  TopLoader.swift
//  MQCodeChallenge
//
//  Created by Apple on 15/06/21.
//

import UIKit
import QuartzCore

class TopLoader: UIRefreshControl {
    
    public var loader : TopLoaderView = {
        let image : UIImage = UIImage(named: "rolling")!
        return TopLoaderView(image: image)
    }()
    
    override init() {
        super.init()
        self.addSubview(loader)
        loader.frame.origin.x = (UIScreen.main.bounds.size.width / 2) - 14
        loader.frame.origin.y = (UIScreen.main.bounds.size.height / 2)
        self.backgroundColor = .clear
        self.tintColor = .clear
    }
    
    // MARK - Init
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}


class TopLoaderView: UIView {
    
    // MARK - Variables
    
    lazy private var animationLayer : CALayer = {
        return CALayer()
    }()
    
    var isAnimating : Bool = false
    var hidesWhenStopped : Bool = true
    
    // MARK - Init
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    init(image : UIImage) {
        let frame : CGRect = CGRect(x: 0.0, y: 0.0, width: 28, height: 28)
        super.init(frame: frame)
        animationLayer.frame = frame
        animationLayer.contents = image.cgImage
        animationLayer.masksToBounds = true
        self.layer.addSublayer(animationLayer)
        addRotation(forLayer: animationLayer)
        pause(layer: animationLayer)
        self.isHidden = true
    }
    // MARK - Func
    func addRotation(forLayer layer : CALayer) {
        let rotation : CABasicAnimation = CABasicAnimation(keyPath:"transform.rotation.z")
        rotation.duration = 1.0
        rotation.isRemovedOnCompletion = false
        rotation.repeatCount = HUGE
        rotation.fillMode = CAMediaTimingFillMode.forwards
        rotation.fromValue = NSNumber(value: 0.0)
        rotation.toValue = NSNumber(value: 3.14 * 2.0)
        layer.cornerRadius = self.frame.size.width / 2
        //FF9300
//        layer.backgroundColor = UIColor.init(hexString: "2694ED", alpha: 1.0)?.cgColor
        layer.add(rotation, forKey: "rotate")
    }
    
    func pause(layer : CALayer) {
        let pausedTime = layer.convertTime(CACurrentMediaTime(), from: nil)
        layer.speed = 0.0
        layer.timeOffset = pausedTime
        isAnimating = false
    }
    
    func resume(layer : CALayer) {
        let pausedTime : CFTimeInterval = layer.timeOffset
        layer.speed = 1.0
        layer.timeOffset = 0.0
        layer.beginTime = 0.0
        
        let timeSincePause = layer.convertTime(CACurrentMediaTime(), from: nil) - pausedTime
        layer.beginTime = timeSincePause
        isAnimating = true
    }
    
    func startAnimating () {
        
        if isAnimating {
            return
        }
        
        if hidesWhenStopped {
            self.isHidden = false
        }
        resume(layer: animationLayer)
    }
    
    func stopAnimating () {
        if hidesWhenStopped {
            self.isHidden = true
        }
        pause(layer: animationLayer)
    }
}
