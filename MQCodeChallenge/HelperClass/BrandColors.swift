//
//  BrandColors.swift
//  MQCodeChallenge
//
//  Created by Apple on 14/06/21.
//

import UIKit

class BrandingColors {
    static let brandColor = #colorLiteral(red: 0, green: 0.7568627451, blue: 0.8352941176, alpha: 1)
    static let navigationColorLightBlue = #colorLiteral(red: 0.62, green: 0.74, blue: 0.76, alpha: 1)

}
