//
//  BaseViewController.swift
//  MQCodeChallenge
//
//  Created by Apple on 14/06/21.
//

import UIKit

class BaseViewController: UIViewController {
    @IBOutlet weak var progressView: UIProgressView!  {
        didSet {
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    func setNavigationBar(bgColor: UIColor, TitleColor: UIColor, title: String,leftbarImageisHide:Bool , rightbarImageisHide:Bool ){
        self.navigationController?.isNavigationBarHidden = false
        self.title = title
        self.navigationController?.navigationBar.isTranslucent = false
        let textAttributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica-Bold", size: 17), NSAttributedString.Key.foregroundColor:TitleColor]
        navigationController?.navigationBar.titleTextAttributes = textAttributes as [NSAttributedString.Key : Any]
        navigationController?.navigationBar.barTintColor = bgColor
        if leftbarImageisHide == false{
            let leftButton = UIBarButtonItem(image: UIImage(named: "back") , style: .plain, target: self, action: #selector(self.back))
            self.navigationItem.leftBarButtonItem  = leftButton
        }
        if rightbarImageisHide == false{
        }
        self.navigationController?.navigationBar.tintColor = UIColor.black;
        self.navigationController?.navigationBar.barTintColor = bgColor
        self.navigationController?.navigationBar.shadowImage = UIImage()
    }
    func hideNavigationBar(hideBar:Bool)  {
        self.navigationController?.isNavigationBarHidden = hideBar
    }
    @objc func back() {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func close() {

    }
    func hidetabbar(tabBarHide:Bool){
        self.tabBarController?.tabBar.isHidden = tabBarHide
    }
}
