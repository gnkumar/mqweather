//
//  LocationTableViewCell.swift
//  MQCodeChallenge
//
//  Created by Apple on 15/06/21.
//

import UIKit

protocol LocationCellDelegate: class {
    func deleteTableViewRow(int: IndexPath)
}
class LocationTableViewCell: UITableViewCell {

    @IBOutlet weak var trashButton: UIButton!
    @IBOutlet weak var locationNameLabel: UILabel!
    var selectedIndex: IndexPath?
    weak var delegate: LocationCellDelegate?
    var viewController: UIViewController!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadCellData(data: LocationData, indePath: IndexPath, vc: UIViewController)   {
        self.selectedIndex = indePath
        locationNameLabel.text = data.locName
        self.viewController = vc
    }
    
    @IBAction func trashButtonAction(_ sender: Any) {
        
                func deleteBookMark() {
                    ShareInstance.shared.locationData.remove(at: selectedIndex?.row ?? 0)
                    self.delegate?.deleteTableViewRow(int: selectedIndex ?? IndexPath.init(index: 0))
                }
                let alert = UIAlertController(title: kSave, message: kDeletionPermission, preferredStyle: .alert)
        
                let yesAction = UIAlertAction(title: kYes, style: .destructive, handler: { action in
                    deleteBookMark()
                })
                let cancel = UIAlertAction(title: kcancel, style: .default, handler: { action in
                })
                alert.addAction(cancel)
                alert.addAction(yesAction)
                alert.view.tintColor = BrandingColors.brandColor
            self.viewController.present(alert, animated: true, completion: nil)
        
        
    }
}
