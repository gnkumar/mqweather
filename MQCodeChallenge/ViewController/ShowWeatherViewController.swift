//
//  ShowWeatherViewController.swift
//  MQCodeChallenge
//
//  Created by Apple on 15/06/21.
//

import UIKit

class ShowWeatherViewController: BaseViewController {

    @IBOutlet weak var windInfoLabel: UILabel!
    @IBOutlet weak var rainChanceLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    public var bookMark: LocationData!
    private let refreshControl = TopLoader()
    @IBOutlet weak var todayDataView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideNavigationBar(hideBar: false)
        self.setNavigationBar(bgColor: BrandingColors.navigationColorLightBlue, TitleColor: UIColor.black, title: "Weather", leftbarImageisHide: false, rightbarImageisHide: true)
        
        refreshControl.tintColor = UIColor.clear
        self.view.addSubview(refreshControl.loader)
        self.view.bringSubviewToFront(refreshControl.loader)
        self.viewUpdate()
        self.serviceCalling()
       
    }
    
    func viewUpdate()  {
        self.todayDataView.layer.cornerRadius = 10
        collectionView.dataSource = self
    }
    
    func serviceCalling()  {
        self.refreshControl.loader.startAnimating()

        let weatherViewModel = WeatherViewModel()
        weatherViewModel.data = self.bookMark
        weatherViewModel.apiCalling()
        weatherViewModel.bindWeatherViewModelToController = {
            DispatchQueue.main.async {
                self.refreshControl.loader.stopAnimating()
                guard let mainData = weatherViewModel.watherData else {
                    Toast.show(message: "Something went wrong...", controller: self)
                    return
                }
                let temp = mainData.main.temp
                let humidity = mainData.main.humidity
                let rainChance = weatherViewModel.watherData.weather[0].main
                let windInfo = weatherViewModel.watherData.wind.speed
                let celsiusTemp = temp - 273.15
               let celsiusString = String(format: "%.0f",celsiusTemp)
                self.windInfoLabel.text = "\(windInfo)"
                self.humidityLabel.text = "\(humidity)"
                self.tempLabel.text = "\(celsiusString)"
                self.rainChanceLabel.text = "\(rainChance)"
            }
        }
    }
}

extension ShowWeatherViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 50
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionCell", for: indexPath)
        return cell
    }
}
