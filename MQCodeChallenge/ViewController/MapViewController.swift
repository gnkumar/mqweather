//
//  MapViewController.swift
//  MQCodeChallenge
//
//  Created by Apple on 15/06/21.
//

import UIKit
import MapKit
import CoreLocation


protocol AddNewBookmarkDelegate: class {
    func newBookMarkedAdded()
}

class MapViewController: BaseViewController, CLLocationManagerDelegate, MKMapViewDelegate {

    let locationManager = CLLocationManager()
    @IBOutlet weak var mapView: MKMapView! {
        didSet {
            mapView.delegate = self
            mapView.mapType = .standard
            mapView.isZoomEnabled = true
            mapView.isScrollEnabled = true
        }
    }
    private let reuseIdentifier = "identifier"
    private var annotation = MKPointAnnotation()
    private var locationData: LocationData?
    weak var delegate: AddNewBookmarkDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideNavigationBar(hideBar: false)
        self.locationAccessRequest()
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(gestureReconizer:)))
        mapView.addGestureRecognizer(gestureRecognizer)
        
        self.setNavigationBar(bgColor: BrandingColors.navigationColorLightBlue, TitleColor: UIColor.black, title: "Map", leftbarImageisHide: false, rightbarImageisHide: true)

        self.addSaveButton()
        
    }
    
    private func addSaveButton() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: kSave, style: .plain, target: self, action: #selector(saveToppedLocation(_:)))
    }
    private func locationAccessRequest() {
        self.locationManager.requestAlwaysAuthorization()
            // For use in foreground
            self.locationManager.requestWhenInUseAuthorization()
            if CLLocationManager.locationServicesEnabled() {
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyBest
                locationManager.startUpdatingLocation()
            }
            if let coor = mapView.userLocation.location?.coordinate{
                mapView.setCenter(coor, animated: true)
            }
    }
    
    @objc func handleTap(gestureReconizer: UITapGestureRecognizer) {

        let location = gestureReconizer.location(in: mapView)
        let coordinate = mapView.convert(location,toCoordinateFrom: mapView)
        let locationCoordinate = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(locationCoordinate) { (placemarks, error) in
            if let places = placemarks {
                for place in places {
                    let locValue:CLLocationCoordinate2D = locationCoordinate.coordinate
                    self.annotation.coordinate = locValue
                    self.annotation.title = place.name ?? ""
                    self.annotation.subtitle = ""
                    self.mapView.addAnnotation(self.annotation)
                    self.locationData = LocationData.init(locLatitude: locValue.latitude, locLongitude: locValue.longitude, name: place.name ?? "")
                }
            }
        }
    }

    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        Toast.show(message: self.annotation.title ?? "", controller: self)
        
    }
    @IBAction func saveToppedLocation(_ sender: UIButton) {
        if let locationData = self.locationData {
            ShareInstance.shared.locationData.append(locationData)
            self.delegate?.newBookMarkedAdded()
            self.navigationController?.popViewController(animated: true)
        } else {
            Toast.show(message: "Please tap on map and select the location.", controller: self)
        }
    }
    
}
