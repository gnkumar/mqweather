//
//  WebViewController.swift
//  MQCodeChallenge
//
//  Created by Apple on 16/06/21.
//

import UIKit
import WebKit

class WebViewController: BaseViewController, WKNavigationDelegate {
   private var webView: WKWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideNavigationBar(hideBar: false)
        self.setNavigationBar(bgColor: BrandingColors.navigationColorLightBlue, TitleColor: UIColor.black, title: "Help", leftbarImageisHide: false, rightbarImageisHide: true)

        webView = WKWebView()
        webView.frame = self.view.frame
        webView.navigationDelegate = self
        self.view.addSubview(webView)
        self.loadJSONFile()
        // Do any additional setup after loading the view.
    }
    private func loadJSONFile() {
        let url = Bundle.main.url(forResource: "help", withExtension: "html")
        webView.load(URLRequest(url: url!))
        webView.allowsBackForwardNavigationGestures = true
    }
    
}
