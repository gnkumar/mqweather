//
//  HomeViewController.swift
//  MQCodeChallenge
//
//  Created by Apple on 14/06/21.
//

import UIKit

class HomeViewController: BaseViewController {

    @IBOutlet weak var locationsTableView: UITableView! {
        didSet {
            locationsTableView.delegate = self
            locationsTableView.dataSource = self
            locationsTableView.tableFooterView = UIView.init(frame: .zero)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideNavigationBar(hideBar: false)
        self.setNavigationBar(bgColor: BrandingColors.navigationColorLightBlue, TitleColor: UIColor.black, title: "Bookmarks", leftbarImageisHide: true, rightbarImageisHide: true)

        locationsTableView.register(UINib.init(nibName: "LocationTableViewCell", bundle: nil), forCellReuseIdentifier: "LocationTableViewCell")
        self.addMapButton()

        if ShareInstance.shared.locationData.count == 0 {
            Toast.show(message: "Bookmarks are not available, Add Bookmarks using map.", controller: self)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.locationsTableView.reloadData()
    }
    
    private func addMapButton() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: kMap, style: .plain, target: self, action: #selector(showMapView))
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: kHelp, style: .plain, target: self, action: #selector(showHelpView))
    }
    
    @objc func showMapView() {
        let mapView = kMainStoryBoard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        mapView.delegate = self
        self.navigationController?.pushViewController(mapView, animated: true)
    }
    @objc func showHelpView() {
        let helpView = kMainStoryBoard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        self.navigationController?.pushViewController(helpView, animated: true)
    }
    
}

extension HomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return ShareInstance.shared.locationData.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LocationTableViewCell") as! LocationTableViewCell
            let locationData = ShareInstance.shared.locationData[indexPath.row]
            cell.delegate = self
            cell.loadCellData(data: locationData, indePath: indexPath, vc: self)
        
        return cell
    }
}

extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let weatherInfo = kMainStoryBoard.instantiateViewController(withIdentifier: "ShowWeatherViewController") as! ShowWeatherViewController
        weatherInfo.bookMark = ShareInstance.shared.locationData[indexPath.row]
        self.navigationController?.pushViewController(weatherInfo, animated: true)
    }
}
extension HomeViewController: AddNewBookmarkDelegate {
    func newBookMarkedAdded() {
        self.locationsTableView.reloadData()
    }
}
extension HomeViewController: LocationCellDelegate {
    func deleteTableViewRow(int: IndexPath) {
        self.locationsTableView.reloadData()
    }
}
