//
//  NetworkService.swift
//  MQCodeChallenge
//
//  Created by Apple on 14/06/21.
//

import Foundation

class NetworkService {
    
    static let shared = NetworkService()
    let BaseURL = "https://api.openweathermap.org/data/2.5/weather?"
    let URL_API_KEY = "65d00499677e59496ca2f318eb68c049"
    let session = URLSession(configuration: .default)
    
    func getWeather(locationData: LocationData, onSuccess: @escaping (Result) -> Void, onError: @escaping (String) -> Void) {
        guard let url = URL(string: "\(BaseURL)lat=\(locationData.lattitude!)&lon=\(locationData.longitude!)&appid=\(URL_API_KEY)") else {
            onError("Error building URL")
            return
        }
        print(url)
        let task = session.dataTask(with: url) { (data, response, error) in
            
            DispatchQueue.main.async {
                if let error = error {
                    onError(error.localizedDescription)
                    return
                }
                
                guard let data = data, let response = response as? HTTPURLResponse else {
                    onError("Invalid data or response")
                    return
                }
                
                do {
                    if response.statusCode == 200 {
                        print(response.statusCode)
                        let items = try JSONDecoder().decode(Result.self, from: data)
                        print(items)
                        onSuccess(items)
                    } else {
                        onError("Response wasn't 200. It was: " + "\n\(response.statusCode)")
                    }
                } catch {
                    onError(error.localizedDescription)
                }
            }
            
        }
        task.resume()
    }
}
   
