//
//  WeatherViewModel.swift
//  MQCodeChallenge
//
//  Created by Apple on 14/06/21.
//

import Foundation

class WeatherViewModel : NSObject {
    private var apiService : NetworkService!
    public var cityName : String!
    public var data: LocationData!
    public var error: String!

    private(set) var watherData : Result! {
        didSet {
            self.bindWeatherViewModelToController()
        }
    }
   
    var bindWeatherViewModelToController : (() -> ()) = {}
    
    override init() {
        super.init()
       
    }
    func apiCalling()  {
        self.apiService =  NetworkService()
        callFuncToGetWeatherData(location: self.data)
    }
    func callFuncToGetWeatherData(location: LocationData) {
        self.apiService.getWeather(locationData: location) { (result) in
            self.watherData = result
        } onError: { (res) in
            print(res)
            self.error = res
        }
    }
    
    
}
