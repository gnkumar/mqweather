//
//  Coord.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on June 14, 2021
//
import Foundation

struct Coord: Codable {

    let lon: Double
    let lat: Double

    private enum CodingKeys: String, CodingKey {
        case lon = "lon"
        case lat = "lat"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        lon = try values.decode(Double.self, forKey: .lon)
        lat = try values.decode(Double.self, forKey: .lat)
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(lon, forKey: .lon)
        try container.encode(lat, forKey: .lat)
    }

}
