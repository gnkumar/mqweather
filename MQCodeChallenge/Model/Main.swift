//
//  Main.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on June 14, 2021
//
import Foundation


struct Main: Codable {

    let temp: Double
    let feelsLike: Double
    let tempMin: Double
    let tempMax: Double
    let pressure: Int
    let humidity: Int
//    let seaLevel: Int
//    let grndLevel: Int

    private enum CodingKeys: String, CodingKey {
        case temp = "temp"
        case feelsLike = "feels_like"
        case tempMin = "temp_min"
        case tempMax = "temp_max"
        case pressure = "pressure"
        case humidity = "humidity"
//        case seaLevel = "sea_level"
//        case grndLevel = "grnd_level"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        temp = try values.decode(Double.self, forKey: .temp)
        feelsLike = try values.decode(Double.self, forKey: .feelsLike)
        tempMin = try values.decode(Double.self, forKey: .tempMin)
        tempMax = try values.decode(Double.self, forKey: .tempMax)
        pressure = try values.decode(Int.self, forKey: .pressure)
        humidity = try values.decode(Int.self, forKey: .humidity)
//        seaLevel = try values.decode(Int.self, forKey: .seaLevel)
//        grndLevel = try values.decode(Int.self, forKey: .grndLevel)
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(temp, forKey: .temp)
        try container.encode(feelsLike, forKey: .feelsLike)
        try container.encode(tempMin, forKey: .tempMin)
        try container.encode(tempMax, forKey: .tempMax)
        try container.encode(pressure, forKey: .pressure)
        try container.encode(humidity, forKey: .humidity)
//        try container.encode(seaLevel, forKey: .seaLevel)
//        try container.encode(grndLevel, forKey: .grndLevel)
    }

}
