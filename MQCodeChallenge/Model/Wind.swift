//
//  Wind.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on June 14, 2021
//
import Foundation
struct Wind: Codable {

    let speed: Double
    let deg: Int
    let gust: Double

    private enum CodingKeys: String, CodingKey {
        case speed = "speed"
        case deg = "deg"
        case gust = "gust"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        speed = try values.decode(Double.self, forKey: .speed)
        deg = try values.decode(Int.self, forKey: .deg)
        gust = try values.decode(Double.self, forKey: .gust)
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(speed, forKey: .speed)
        try container.encode(deg, forKey: .deg)
        try container.encode(gust, forKey: .gust)
    }

}
