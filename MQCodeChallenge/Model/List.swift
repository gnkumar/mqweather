//
//  List.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on June 16, 2021
//
import Foundation

struct List: Codable {

	let dt: Int
	let main: Main
	let weather: [Weather]
	let clouds: Clouds
	let wind: Wind
	let visibility: Int
	let pop: Int
	let sys: Sys
	let dtTxt: String

	private enum CodingKeys: String, CodingKey {
		case dt = "dt"
		case main = "main"
		case weather = "weather"
		case clouds = "clouds"
		case wind = "wind"
		case visibility = "visibility"
		case pop = "pop"
		case sys = "sys"
		case dtTxt = "dt_txt"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		dt = try values.decode(Int.self, forKey: .dt)
		main = try values.decode(Main.self, forKey: .main)
		weather = try values.decode([Weather].self, forKey: .weather)
		clouds = try values.decode(Clouds.self, forKey: .clouds)
		wind = try values.decode(Wind.self, forKey: .wind)
		visibility = try values.decode(Int.self, forKey: .visibility)
		pop = try values.decode(Int.self, forKey: .pop)
		sys = try values.decode(Sys.self, forKey: .sys)
		dtTxt = try values.decode(String.self, forKey: .dtTxt)
	}

	func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(dt, forKey: .dt)
		try container.encode(main, forKey: .main)
		try container.encode(weather, forKey: .weather)
		try container.encode(clouds, forKey: .clouds)
		try container.encode(wind, forKey: .wind)
		try container.encode(visibility, forKey: .visibility)
		try container.encode(pop, forKey: .pop)
		try container.encode(sys, forKey: .sys)
		try container.encode(dtTxt, forKey: .dtTxt)
	}

}