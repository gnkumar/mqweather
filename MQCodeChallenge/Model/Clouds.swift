//
//  Clouds.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on June 14, 2021
//
import Foundation


struct Clouds: Codable {

    let all: Int

    private enum CodingKeys: String, CodingKey {
        case all = "all"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        all = try values.decode(Int.self, forKey: .all)
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(all, forKey: .all)
    }

}
