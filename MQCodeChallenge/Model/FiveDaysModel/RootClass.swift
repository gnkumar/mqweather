//
//  RootClass.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on June 16, 2021
//
import Foundation

struct RootObject: Codable {

    let cod: String
    let message: Int
    let cnt: Int
    let list: [List]
    let city: City

    private enum CodingKeys: String, CodingKey {
        case cod = "cod"
        case message = "message"
        case cnt = "cnt"
        case list = "list"
        case city = "city"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        cod = try values.decode(String.self, forKey: .cod)
        message = try values.decode(Int.self, forKey: .message)
        cnt = try values.decode(Int.self, forKey: .cnt)
        list = try values.decode([List].self, forKey: .list)
        city = try values.decode(City.self, forKey: .city)
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(cod, forKey: .cod)
        try container.encode(message, forKey: .message)
        try container.encode(cnt, forKey: .cnt)
        try container.encode(list, forKey: .list)
        try container.encode(city, forKey: .city)
    }

}
//struct List: Codable {
//
//    let dt: Int
//    let main: Main
//    let weather: [Weather]
//    let clouds: Clouds
//    let wind: Wind
//    let visibility: Int
//    let pop: Int
//    let sys: Sys
//    let dtTxt: String
//
//    private enum CodingKeys: String, CodingKey {
//        case dt = "dt"
//        case main = "main"
//        case weather = "weather"
//        case clouds = "clouds"
//        case wind = "wind"
//        case visibility = "visibility"
//        case pop = "pop"
//        case sys = "sys"
//        case dtTxt = "dt_txt"
//    }
//
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        dt = try values.decode(Int.self, forKey: .dt)
//        main = try values.decode(Main.self, forKey: .main)
//        weather = try values.decode([Weather].self, forKey: .weather)
//        clouds = try values.decode(Clouds.self, forKey: .clouds)
//        wind = try values.decode(Wind.self, forKey: .wind)
//        visibility = try values.decode(Int.self, forKey: .visibility)
//        pop = try values.decode(Int.self, forKey: .pop)
//        sys = try values.decode(Sys.self, forKey: .sys)
//        dtTxt = try values.decode(String.self, forKey: .dtTxt)
//    }
//
//    func encode(to encoder: Encoder) throws {
//        var container = encoder.container(keyedBy: CodingKeys.self)
//        try container.encode(dt, forKey: .dt)
//        try container.encode(main, forKey: .main)
//        try container.encode(weather, forKey: .weather)
//        try container.encode(clouds, forKey: .clouds)
//        try container.encode(wind, forKey: .wind)
//        try container.encode(visibility, forKey: .visibility)
//        try container.encode(pop, forKey: .pop)
//        try container.encode(sys, forKey: .sys)
//        try container.encode(dtTxt, forKey: .dtTxt)
//    }
//
//}
struct City: Codable {

    let id: Int
    let name: String
    let coord: Coord
    let country: String
    let population: Int
    let timezone: Int
    let sunrise: Int
    let sunset: Int

    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case coord = "coord"
        case country = "country"
        case population = "population"
        case timezone = "timezone"
        case sunrise = "sunrise"
        case sunset = "sunset"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(Int.self, forKey: .id)
        name = try values.decode(String.self, forKey: .name)
        coord = try values.decode(Coord.self, forKey: .coord)
        country = try values.decode(String.self, forKey: .country)
        population = try values.decode(Int.self, forKey: .population)
        timezone = try values.decode(Int.self, forKey: .timezone)
        sunrise = try values.decode(Int.self, forKey: .sunrise)
        sunset = try values.decode(Int.self, forKey: .sunset)
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(name, forKey: .name)
        try container.encode(coord, forKey: .coord)
        try container.encode(country, forKey: .country)
        try container.encode(population, forKey: .population)
        try container.encode(timezone, forKey: .timezone)
        try container.encode(sunrise, forKey: .sunrise)
        try container.encode(sunset, forKey: .sunset)
    }

}
