//
//  RootClass.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on June 14, 2021
//
import Foundation

struct Result: Codable {
    
    let coord: Coord
    let weather: [Weather]
    let base: String
    let main: Main
    let visibility: Int
    let wind: Wind
    let clouds: Clouds
    let dt: Int
    let sys: Sys
    let timezone: Int
    let id: Int
    let name: String
    let cod: Int

    private enum CodingKeys: String, CodingKey {
        case coord = "coord"
        case weather = "weather"
        case base = "base"
        case main = "main"
        case visibility = "visibility"
        case wind = "wind"
        case clouds = "clouds"
        case dt = "dt"
        case sys = "sys"
        case timezone = "timezone"
        case id = "id"
        case name = "name"
        case cod = "cod"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        coord = try values.decode(Coord.self, forKey: .coord)
        weather = try values.decode([Weather].self, forKey: .weather)
        base = try values.decode(String.self, forKey: .base)
        main = try values.decode(Main.self, forKey: .main)
        visibility = try values.decode(Int.self, forKey: .visibility)
        wind = try values.decode(Wind.self, forKey: .wind)
        clouds = try values.decode(Clouds.self, forKey: .clouds)
        dt = try values.decode(Int.self, forKey: .dt)
        sys = try values.decode(Sys.self, forKey: .sys)
        timezone = try values.decode(Int.self, forKey: .timezone)
        id = try values.decode(Int.self, forKey: .id)
        name = try values.decode(String.self, forKey: .name)
        cod = try values.decode(Int.self, forKey: .cod)
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(coord, forKey: .coord)
        try container.encode(weather, forKey: .weather)
        try container.encode(base, forKey: .base)
        try container.encode(main, forKey: .main)
        try container.encode(visibility, forKey: .visibility)
        try container.encode(wind, forKey: .wind)
        try container.encode(clouds, forKey: .clouds)
        try container.encode(dt, forKey: .dt)
        try container.encode(sys, forKey: .sys)
        try container.encode(timezone, forKey: .timezone)
        try container.encode(id, forKey: .id)
        try container.encode(name, forKey: .name)
        try container.encode(cod, forKey: .cod)
    }

}
